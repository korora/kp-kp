%define git 7be6d88

Name:           kp
Version:        0.1
Release:        2%{?dist}
Summary:        Korora Packaging Tool
License:        GPLv3
URL:            https://kororaproject.org/
Source0:        https://gitlab.com/korora/kp/-/archive/master/kp-master.tar.gz
BuildArch:      noarch
Requires:       createrepo mock livecd-tools pykickstart rpm-sign

%description
Korora Package tool (called kp) is a bunch of shell scripts that wrap standard system commands 
(like git, mock and livecd-creator) to build Korora packages and images. 
Users should be running Korora or Fedora already, however which specific version generally doesn't matter.

%prep
%setup -q -n kp-master

%build

%install
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_bindir}

ls

install -m 755 kp %{buildroot}%{_bindir}/kp
cp -r lib/* %{buildroot}%{_datadir}/%{name}/

%post

%postun

%files
%doc COPYING
%{_bindir}/kp
%{_datadir}/kp

%changelog
* Sat Jul 21 2018 JMiahMan <JMiahMan@unity-linux.org> 0.1-2
- Don't install into /usr/share/share
- Grab newer version from git

* Mon Jan 11 2016 Jim Dean <ozjd@kororaproject.org> 0.1-1
- Initial spec. 
